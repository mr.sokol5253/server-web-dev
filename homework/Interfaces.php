<?php
interface Figures
{
    public function __construct($width, $height);
    public function CalculateSquare();
}

class Square implements Figures {
    public function __construct($width, $height) {
        $this->width = $width;
        $this->height = $height;
    }
    public function CalculateSquare() {
        return $this->height * $this->width;
    }
}

class Triangle implements Figures {
    public function __construct($width, $height) {
        $this->width = $width;
        $this->height = $height;
    }
    public function CalculateSquare() {
        return "Объект класса " . get_class($this) . " не реализует метод CalculateSquare";
    }
}

$i = new Square(10, 20);
echo $i->CalculateSquare();
echo '<br>';
$i = new Triangle(10, 20);
echo $i->CalculateSquare();
?>