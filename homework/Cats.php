<?php
class Cats
{
    private $name;
    private $color;

    public function __construct(string $name, string $color)
    {
        $this->name = $name;
        $this->color = $color;
    }

    public function sayHello()
    {
        return 'Привет, меня зовут '.$this->name.', мой цвет '.$this->color;
    }
    public function __get($color) {
        return $this->color;
    }
}

$i = new Cats("James", "Красный");
echo $i->sayHello();
echo "<br>".$i->color;
?>