<?php
class Lesson
{
    private $array = [];

    public function __construct(string $title, string $text, string $homework)
    {
        $this->array["title"] = $title;
        $this->array["text"] = $text;
        $this->array["homework"] = $homework;
    }
}

class PaidLesson extends Lesson {
    private $array = [];
    public function __construct(string $title, string $text, string $homework, float $price = 0)
    {
        $this->array["title"] = $title;
        $this->array["text"] = $text;
        $this->array["homework"] = $homework;
        $this->array["price"] = $price;

    }
    function __set($price, $value) {
        $this->array[$price] = $value;
    }
    function __get($price) {
        return $this->array[$price];
    }
}

$i = new PaidLesson("Урок о наследовании PHP", "Лол, кек чебурек", "Ложитесь спать, утро вечера мудренее");
$i->price = 99.9;
echo $i->price;
echo "<br>";
var_dump($i);
?>