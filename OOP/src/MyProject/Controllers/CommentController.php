<?php
    namespace MyProject\Controllers;
    use MyProject\Models\Articles\Article;
    use MyProject\Controllers\ArticleController;
    use MyProject\Models\Users\User;
    use MyProject\Models\Comments\Comment;
    use MyProject\View\View;


    class CommentController{
        private $view;

        public function __construct(){
            $this->view = new View(__DIR__.'/../../../templates');
        }
        public function view(int $articleId){
            $article = Article::getById($articleId);
            $comments = Comment::getByArticleId($articleId);
            $this->view->renderHtml('comments/view.php', ['comments' => $comments, 'article' => $article, 'User' => User::class]);
        }
        public function delete(int $articleId, $commentId){
            $this->view->renderHtml('comments/deleted.php');
            $comment = Comment::getById($commentId);
            $comment->delete();
            $articleController = new ArticleController();
            $articleController->view($articleId);
        }
        public function add(int $articleId){
            $this->view->renderHtml('comments/added.php');
            $comment = new Comment();
            $author = User::getByName($_POST['name']);
            if ($author == NULL) {
                $author = new User();
                $author->setName($_POST['name']);
                $author->save();
                $author = User::getByName($_POST['name']);
            }
            $comment->setAuthorId($author);
            $comment->setText($_POST['text']);
            $comment->setArticleId($articleId);
            
            $comment->save();
            $articleController = new ArticleController();
            $articleController->view($articleId);
        }
        public function edit(int $articleId, int $commentId){
            $comment = Comment::getById($commentId);
            $article = Article::getById($articleId);
            if (!empty($_POST)) {
                $author = User::getByName($_POST['name']);
                if ($author == NULL) {
                    $author = new User();
                    $author->setName($_POST['name']);
                    $author->save();
                    $author = User::getByName($_POST['name']);
                }
                $comment->setAuthorId($author);
                $comment->setText($_POST['text']);
                $comment->setArticleId($articleId);
                $this->view->renderHtml('comments/edited.php');
                $comment->save();
            }
            $this->view->renderHtml('comments/edit.php', ['comment' => $comment, 'article' => $article, 'User' => User::class]);
        }
    }    
?>