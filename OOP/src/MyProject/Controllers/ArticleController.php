<?php
    namespace MyProject\Controllers;
    use MyProject\Models\Articles\Article;
    use MyProject\Models\Users\User;
    use MyProject\View\View;
    use MyProject\Controllers\CommentController;

    class ArticleController{
        private $view;

        public function __construct(){
            $this->view = new View(__DIR__.'/../../../templates');
        }
        public function view(int $articleId){
            $article = Article::getById($articleId);
            $commentController = new CommentController();
            $reflector = new \ReflectionObject($article);
            $properties = $reflector->getProperties();
            $propertiesName = [];
            foreach($properties as $property){
                $propertiesName[] = $property->getName(); 
            }
            if ($article === null){
                $this->view->renderHtml('errors/404.php', [], 404);
                return;
            }
            $this->view->renderHtml('articles/view.php', ['article' => $article]);
            $commentController->view($articleId);
        }

        public function edit(int $articleId): void
        {
            $article = Article::getById($articleId);
            if ($article === null){
                $this->view->renderHtml('errors/404.php', [], 404);
                return;
            }
            $article->setName($_POST['title']);
            $article->setText($_POST['text']);
            $article->save();
            $this->view->renderHtml('success.php');
        }
        public function add(): void{
            $author = User::getById(1);
            $article = new Article();
            $article->setAuthorId($author);
            $article->setName($_POST['title']);
            $article->setText($_POST['text']);
            $article->save();
            $this->view->renderHtml('success.php');
        }
        public function delete(int $articleId):void{
            $article = Article::getById($articleId);
            if ($article === null){
                $this->view->renderHtml('errors/404.php', [], 404);
                return;
            }
            $this->view->renderHtml('success.php');
            $article->delete();
        }
        public function create():void{
            $this->view->renderHtml('articles/create.php');
        }
    }
?>