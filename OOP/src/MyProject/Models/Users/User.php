<?php
    namespace MyProject\Models\Users;
    use MyProject\Models\ActiveRecordEntity;
    use MyProject\Services\Db;

    
    class User extends ActiveRecordEntity{
        protected $id;
        protected $name;

        public function getName(): string{
            return $this->name;
        }
        public function setName(string $name){
            $this->name = $name;
        }
        public static function getByName(string $name): ?self{
            $db = Db::getInstance();
            $sql = 'SELECT * FROM `User` WHERE name=:AuthorName';
            $user = $db->query($sql, [':AuthorName' => $name], User::class );
            return $user ? $user[0] : null;
        }
        public static function getTableName(): string{
            return 'User';
        }
    }
?>