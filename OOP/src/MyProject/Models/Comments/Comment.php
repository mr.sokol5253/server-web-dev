<?php
    namespace MyProject\Models\Comments;
    use MyProject\Models\ActiveRecordEntity;
    use MyProject\Models\Users\User;

    
    class Comment extends ActiveRecordEntity{
        protected $id;    
        protected $authorId;
        protected $articleId;
        protected $text;
        protected $date;

        public function getAuthorId(){
            return $this->authorId;
        }
        public function getText(){
            return $this->text;
        }
        public static function getTableName(): string{
            return 'comments';
        }
        public function setText(string $text){
            $this->text = $text;
        }
        public function setAuthorId(User $author){
            $this->authorId = $author->getId();
        }
        public function setArticleId(string $articleId){
            $this->articleId = $articleId;
        }
    }
?>