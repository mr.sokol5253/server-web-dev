<div style='max-width: 800px; margin-left: auto; margin-right: auto'>
    <h3 style='margin-top: 50px;'>Комментарии</h3>
    <hr>   
    <?php if($comments!==NULL) foreach($comments as $comment):?>
        <?php $last_id = $comment->getId(); ?>
            <h4 id="comment<?= $comment->getId(); ?>" style='display: inline-block; width: 400px'><?= $User::getById($comment->getAuthorId())->getName(); ?></h4>
            <a style='display: inline-block; width: 396px; text-align: right;' href="/OOP/www/article/<?= $article->getId()?>/comments/<?=$comment->getId();?>/edit">Редактировать</a>
            <p><?= $comment->getText();?></p>
            <a href="/OOP/www/article/<?= $article->getId()?>/comments/<?=$comment->getId();?>/delete" style='display: block; width: 100%; text-align: right; color: red;'>Удалить</a>
            <hr>   
    <?php endforeach;?>
</div>
<form style='max-width: 800px; margin-left: auto; margin-right: auto;' action="/OOP/www/article/<?= $article->getId()?>/comments#comment<?=$last_id+1 ?>" method="POST">
    <input name='name' style='display: block; height: 25px; width: 400px; margin-left: auto; margin-right: auto; margin-bottom: 10px' type="text" placeholder='Имя' required>
    <input name='text' style='display: block; height: 25px; width: 400px; margin-left: auto; margin-right: auto; margin-bottom: 10px' type="text" placeholder='Текст комментария' required>
    <input type="submit" style='display: block; height: 30px; width: 410px; margin-left: auto; margin-right: auto' value='Отправить комментарий'>
</form>