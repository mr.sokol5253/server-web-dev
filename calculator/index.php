<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Calculator</title>
    <style>
        .invisible {
            display: none;
        }
        .form {
            display: block;
            margin-top: 20px;
            height: 30px;
        }
        .form input[type="text"] {
            width: 250px;
            height: 80%;
        }
        .form input[type="button"], .form input[type="submit"] {
            height: 100%;
        }
    </style>
</head>
<body>
    <header>
        <h1>Calculator</h1>
    </header>
    <?php 

            function ctg($value) {
                
                $cos = cos($value);
                $sin = sin($value);
                if ($sin!=0) {
                    $ctg = $cos / $sin;
                }
                else {
                    return null;
                }
                return $ctg;
            }


            require("trig.txt");
            if (isset($_POST['input'])) {
                $expression = $_POST['input'];
            }
            else{
                $expression = '';
            }
            function ConvertToRPN($expression){
                $stack = "";
                $RPN = array_fill(0, strlen($expression), "");
                $k = 0;
                for($i=0; $i<strlen($expression); $i++){
                    if (strlen($stack)==0 && priority($expression[$i]) != 0 ){
                        $stack = $stack . $expression[$i];
                    }
                    else if (priority($expression[$i]) == 4){
                        while ($stack[strlen($stack)-1] != '(') {
                            $RPN[$k] = $stack[strlen($stack)-1];
                            $k++;
                            $stack = substr($stack, 0, -1);
                        }
                        $stack = substr($stack, 0, -1);
                    }
                    else if (priority($expression[$i]) == 3){
                        if (priority($stack[strlen($stack)-1]) < 3){
                            $stack = $stack . $expression[$i];
                        }
                        else{
                            while (priority($stack[strlen($stack)-1]) == 3){
                                $RPN[$k] = $stack[strlen($stack)-1];
                                $k++;
                                $stack = substr($stack, 0, -1);
                                if (strlen($stack) == 0){
                                    break;
                                }
                            }
                            $stack = $stack .  $expression[$i];
                        }
                    }
                    else if (priority($expression[$i]) == 2){
                        if (priority($stack[strlen($stack)-1]) < 2){
                            $stack = $stack .  $expression[$i];
                        }
                        else{
                            while (priority($stack[strlen($stack)-1]) >= 2){
                                $RPN[$k] = $stack[strlen($stack)-1];
                                $k++;
                                $stack = substr($stack, 0,-1);
                                if (strlen($stack) == 0){
                                    break;
                                }
                            }
                            $stack = $stack . $expression[$i];
                        }
                    }
                    else if (priority($expression[$i]) == 1){
                        $stack = $stack .  $expression[$i];
                    }
                    else {
                            while (priority($expression[$i]) == 0){
                                $RPN[$k] = $RPN[$k] . $expression[$i];
                                $i++;
                                if ($i >= strlen($expression)){
                                    break;
                                }
                            }
                            $i--;
                            $k++;  
                        }
                    }
                
                while (!empty($stack)){
                    $RPN[$k] = $stack[strlen($stack)-1];
                    $k++;
                    $stack = substr($stack, 0, -1);
                }
                return $RPN;
            }
            function priority($a){
                if ($a == "*" || $a == "/"){
                    return 3;
                }
                else if ($a == '+' || $a == '-'){
                    return 2;
                }
                else if ($a == '('){
                    return 1;
                }
                else if ($a == ')'){
                    return 4;
                }
                else {
                    return 0;
                }
            }

            function CalculateRPN($arr){
                for ($i=0; $i<Count($arr); $i++){
                    if ($arr[$i] == "+"){
                        $a = null;
                        $b = null;
                        for($j = $i-1; $j >= 0; $j--){
                            if ($arr[$j] != "$"){
                                if ($a == null){
                                    $a = $arr[$j];
                                    $arr[$j]="$";
                                }
                                else {
                                    $b = $arr[$j];
                                    $arr[$j]="$";
                                    break;
                                }
                            }
                        }
                        if ($a != null && $b != null){
                            $arr[$i] = $b+$a;
                        }
                        else {
                            $arr[$i] = 0+$a;
                        }
                    }
                    else if ($arr[$i] == "*"){
                        $a = null;
                        $b = null;
                        for($j = $i-1; $j >= 0; $j--){
                            if ($arr[$j] != "$"){
                                if ($a == null){
                                    $a = $arr[$j];
                                    $arr[$j]="$";
                                }
                                else {
                                    $b = $arr[$j];
                                    $arr[$j]="$";
                                    break;
                                }
                            }
                        }
                        if ($a != null && $b != null){
                            $arr[$i] = $a*$b;
                        }
                        else {
                            $arr[$i] = 0*$a;
                        }
                    }
                    else if ($arr[$i] == "/"){
                        $a = null;
                        $b = null;
                        for($j = $i-1; $j >= 0; $j--){
                            if ($arr[$j] != "$"){
                                if ($a == null){
                                    $a = $arr[$j];
                                    $arr[$j]="$";
                                }
                                else {
                                    $b = $arr[$j];
                                    $arr[$j]="$";
                                    break;
                                }
                            }
                        }
                        if ($a != null && $b != null){
                            $arr[$i] = $b/$a;
                        }
                        else {
                            $arr[$i] = 0/$a;
                        }
                    }
                    else if ($arr[$i] == "-"){
                        $a = null;
                        $b = null;
                        for($j = $i-1; $j >= 0; $j--){
                            if ($arr[$j] != "$"){
                                if ($a == null){
                                    $a = $arr[$j];
                                    $arr[$j]="$";
                                }
                                else {
                                    $b = $arr[$j];
                                    $arr[$j]="$";
                                    break;
                                }
                            }
                        }
                        if ($a != null && $b != null){
                            $arr[$i] = $b-$a;
                        }
                        else {
                            $arr[$i] = $a-0;
                        }
                    }
                }
                
                foreach ($arr as $i){
                    if ($i != "$"){
                        return $i;
                    }
                }
                return 0;
            }
        ?>
    <main id='main'>
        <form class='form' action='index.php' method='POST'>
            <input id='input' type='text' name='input'
            <?php
            $first_expression = $expression;            
            trig_parser($expression);
            $calculated = false;
                if (isset($_POST['input'])){
                    $calculated = true;
                    if ($expression[0] == "-") {
                        $expression = "0" . $expression;
                    }
                    for ($i = 0; $i < strlen($expression); $i++){
                        if ($expression[$i] == "="){
                            echo "value=$expression";
                            break;
                        }
                        else if ($i == strlen($expression)-1) {
                            $RPN = ConvertToRPN($expression);
                            $answer = CalculateRPN($RPN);
                            echo "value=" . $first_expression . "=" . $answer;
                        }
                    }
                }
            ?> required>
            <input id='submit-btn' value='Посчитать' type="submit">
            <input <?php 
            $calculated == true;
            if ($calculated == false){
                echo 'class="invisible"';
            }
            ?> value='сбросить' id='reset-btn' type='button' onclick="document.location='index.php'">
        </form>
    </main>
    <footer>
        <h2>Инструкция:</h2>
        <p>Калькулятор может посчитать любой валидный пример состоящий из цифр, скобок и операторов сложения, вычитания, умножения, деления.</p>
        <p>Также он может посчитать тригонометрические функции синуса, косинуса, тангенса, котангенса.</p>
        <p>Для этого используйте следующие формы записи соответсвтенно: sin(знач), cos(знач), tan(знач), ctg(знач)</p>
        <p>Нажмите посчитать один раз, чтобы узнать ответ на все выражение. Второе нажатие преобразует введенные вами функции в цифровые значения</p>
        <p>Пример ввода: (8+10)*cos(0)-tan(3.14)</p>
        <p>Первое нажатие: (8+10)*cos(0)-tan(3.14)=18</p>
        <p>Второе нажатие: (8+10)*(1)-(-0)=18</p>
    </footer>
    <script src="script.js"></script>
</body>
</html>