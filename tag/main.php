<?php
    mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
    $my_db = mysqli_connect('std-mysql', 'std_1867_database', 'Vlad5253sokol.ru', 'std_1867_database');
    if (mysqli_connect_errno()) echo 'Ошибка соединения с БД'. mysqli_connect_error();

    #Получить имя пользователя, из его ID
    function getUserNameById ($id) {
        $the_db = mysqli_connect('std-mysql', 'std_1867_database', 'Vlad5253sokol.ru', 'std_1867_database');
        $the_sql = 'SELECT * FROM `Users` WHERE id="'.$id.'";';
        $the_sql_res = mysqli_query($the_db, $the_sql);
        $the_row = mysqli_fetch_assoc($the_sql_res);
        return $the_row["name"];
    }

    #Получить имя хэштега из его ID
    function getTagNameById ($id) {
        $the_db = mysqli_connect('std-mysql', 'std_1867_database', 'Vlad5253sokol.ru', 'std_1867_database');
        $the_sql = 'SELECT * FROM `#` WHERE id="'.$id.'";';
        $the_sql_res = mysqli_query($the_db, $the_sql);
        $the_row = mysqli_fetch_assoc($the_sql_res);
        return $the_row["data"];
    }

    #Получить id хэштега по имени
    function getTagIdByName ($name) {
        $the_db = mysqli_connect('std-mysql', 'std_1867_database', 'Vlad5253sokol.ru', 'std_1867_database');
        $the_sql = 'SELECT * FROM `#` WHERE data="'.$name.'";';
        $the_sql_res = mysqli_query($the_db, $the_sql);
        $the_row = mysqli_fetch_assoc($the_sql_res);
        return $the_row["id"];
    }
?>
<div class="row">
    <form class="col-md-2 d-flex flex-column mx-2 flex-5" name="SMS_form" method="get">
        <label class="mx-2" for="tag">Хештег</label>
        <input id="tag" class="form-control m-2" name="tag" placeholder="#example"
        <?php
        if (isset($_GET["tag"]) && $_GET["tag"]!=""){
            echo "value='".$_GET["tag"]."'";
        }
        ?>
        type="text">
        <label class="mx-2" for="channel">Канал</label>
        <input id="channel" class="form-control m-2" name="channel"
        <?php
        if (isset($_GET["channel"]) && $_GET["channel"]!=""){
            echo "value='".$_GET["channel"]."'";
        }
        ?>
        type="text">
        <input class="btn btn-primary m-2 col-12" type="submit" value="Найти">
        <?php
        if ((isset($_GET["channel"]) && $_GET["channel"]!="") || (isset($_GET["tag"]) && $_GET["tag"]!="")){
            echo '<a class="btn btn-primary m-2 col-12" href="../tag">Очистить</a>';
        }
        ?>  
    </form>
    <div class="col-sm col-12">
        <?php
        $tag_id = getTagIdByName($_GET['tag']);
        $sql = 'SELECT * FROM `SMS`';
        if ((isset($_GET["channel"]) && $_GET["channel"]!="") && (isset($_GET["tag"]) && $_GET["tag"]!="")){
            $sql = $sql . ' WHERE channel_name="'.$_GET["channel"].'" AND `#_id`="'.$tag_id.'";';
            echo "<p class='m-3'><b>Сообщения из канала: ".$_GET["channel"]."</b></p>";
            echo "<p class='m-3'><b>Фильтр по хэштегу: ".$_GET["tag"]."</b></p>";
        }
        else if (isset($_GET["channel"]) && $_GET["channel"]!=""){
            $sql = $sql . ' WHERE `channel_name`="'.$_GET["channel"].'";';
        }
        else if (isset($_GET["tag"]) && $_GET["tag"]!=""){
            $sql = $sql . ' WHERE `#_id`="'.$tag_id.'";';
        }

        $sql_res = mysqli_query($my_db, $sql);
        echo '<div class="row">';
        while ($row = mysqli_fetch_assoc($sql_res)) {
            echo '<div class="col-12 m-1">';
                echo '<div class="card">';
                    echo '<div class="card-body">';
                        echo '<h5 class="card-title">Пользователь: '.getUserNameById($row["user_id"]).'</h5>';
                        echo '<p class="card-subtitle">Канал: <b> "'.$row['channel_name'].'"</b></p>';
                        echo '<p class="card-text">Сообщение:</p>';
                        echo '<p class="card-text m-5">'.$row['sms_text'].'</p>';
                        if (getTagNameById($row["#_id"])!=""){
                            echo '<a href="?tag=%23'.substr(getTagNameById($row["#_id"]), 1).'" class="btn btn-primary">'.getTagNameById($row["#_id"]).'</a>';
                        }
                    echo '</div>';
                echo '</div>';
            echo '</div>';
        }
        echo '</div>';
        ?>
    </div>
</div>