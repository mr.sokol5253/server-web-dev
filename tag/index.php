<!DOCTYPE html>
<html lang="en">
<head>
    <?php
        if (!isset($_GET["page"])) {
            $page = "Main";
        }
        else {
            $page = $_GET["page"];
        }
    ?>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
    <title>
        <?=$page ?>
    </title>
</head>
<body>
    <div class="container">
        <div class="row justify-content-center">
            <a href="?page=Main" class="btn btn-primary m-3 btn-lg col-2" <?php if ($page == "Main") echo "style='display: none;'"?>>Main</a>
            <a href="?page=SMS" class="btn btn-primary m-3 btn-lg col-2" <?php if ($page == "SMS") echo "style='display: none;'"?>>SMS</a>
            <?php
                if ($page == "Main") include "main.php";
                if ($page == "SMS") include "form.php";
            ?>
        </div>
    </div>
</body>
</html>