<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
        form {
            display: flex;
            align-items: flex-start;
            
        }
        textarea {
            resize: none;
            height: 30px;
            font-size: 25px;
            width: 180px;
        }
        input[type="submit"] {
            height: 36px;
        }
    </style>
</head>
<body>
    <form method="POST">
        <textarea name="equation"></textarea>
        <input type="submit" value="Решить" name="solve"/>
    </form>
    <p>
        <?php
            if(array_key_exists('solve', $_POST)) {
                click();
            }
            function click(){
                $equation = $_POST['equation'];
                $equation_array = str_split($equation);
                for ($i = 0; $i < count($equation_array); $i++){
                    if ($equation_array[$i]=="+" or $equation_array[$i] =="-" or $equation_array[$i] =="*" or $equation_array[$i] =="/"){
                        $op = $equation_array[$i].$i;
                    }
                    else if ($equation_array[$i]=="x" or $equation_array[$i]=="X"){
                        $x = $i;
                    }
                    else if ($equation_array[$i]=="="){
                        $equals = $i;
                    }
                }
                # Выше разбил массив значений на OP операторв (в него записал сам оператор и его положение)
                # записал положение знака равно в eqauls и положение икса в x
                echo $equation."<br>";
                solve($op, $equals, $x, $equation_array);
            }

            function solve($op, $equals, $x, $equation_array){
                # _______ НАХОЖУ, ЧТО ПОСЛЕ ЗНАКА РАВНО ________
                $ending = "";
                for ($i = $equals+1; $i < count($equation_array); $i++){
                    $ending = $ending.$equation_array[$i];
                }

                # _______Действия при операторе + ______________
                if (str_split($op)[0] == "+"){
                    $izv_chast = "";
                    if ($x > str_split($op)[1]){ #Сравниваю позицию X и +
                        for ($i=0; $i<str_split($op)[1]; $i++){
                            $izv_chast = $izv_chast.$equation_array[$i]; #Записываю известное значение, если оно до +
                        }
                    }
                    else {
                        for ($i=str_split($op)[1]+1; $i<$equals; $i++){
                            $izv_chast = $izv_chast.$equation_array[$i]; #Записываю известное значение, если оно после +
                        }
                    }
                    $result = $ending - $izv_chast;
                }

                #________Действия при операторе - ______________
                else if (str_split($op)[0] == "-"){
                    $izv_chast = "";
                    if ($x > str_split($op)[1]){ #Сравниваю позицию X и -
                        for ($i=0; $i<str_split($op)[1]; $i++){
                            $izv_chast = $izv_chast.$equation_array[$i]; #Записываю известное значение, если оно до -
                        }
                        $result = $izv_chast- $ending;
                    }
                    else {
                        for ($i=str_split($op)[1]+1; $i<$equals; $i++){
                            $izv_chast = $izv_chast.$equation_array[$i]; #Записываю известное значение, если оно после -
                        }
                        $result = $izv_chast + $ending;
                    }   
                }

                #________Действия при операторе * ______________
                else if (str_split($op)[0] == "*"){
                    $izv_chast = "";
                    if ($x > str_split($op)[1]){ #Сравниваю позицию X и *
                        for ($i=0; $i<str_split($op)[1]; $i++){
                            $izv_chast = $izv_chast.$equation_array[$i]; #Записываю известное значение, если оно до *
                        }
                    }
                    else {
                        for ($i=str_split($op)[1]+1; $i<$equals; $i++){
                            $izv_chast = $izv_chast.$equation_array[$i]; #Записываю известное значение, если оно после *
                        }
                    }
                    $result = $ending / $izv_chast;
                }

                #________Действия при операторе / ______________
                else if (str_split($op)[0] == "/"){
                    $izv_chast = "";
                    if ($x > str_split($op)[1]){ #Сравниваю позицию X и *
                        for ($i=0; $i<str_split($op)[1]; $i++){
                            $izv_chast = $izv_chast.$equation_array[$i]; #Записываю известное значение, если оно до /
                        }
                        $result = $izv_chast / $ending;
                    }
                    else {
                        for ($i=str_split($op)[1]+1; $i<$equals; $i++){
                            $izv_chast = $izv_chast.$equation_array[$i]; #Записываю известное значение, если оно после /
                        }
                        $result = $izv_chast * $ending;
                    }
                    
                }


                echo "x = ".$result;
            }

        ?>
    </p>
</body>
</html>