<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href='./scss/style.css' rel='stylesheet'>
    <title>Document</title>
</head>
<body>
    <header>
        <img class='logo' src='img/LogoMospolytech.jpg' alt="Логотип">
        <h1>"Hello, World!"</h1>
    </header>
    <main>
        <div class='content'>
            <?php
            echo date('l jS \of F Y h:i:s A');
            ?>
        </div>
    </main>
    <footer>

    </footer>
</body>
</html>