<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('template', ['page'=>'main']);
});

Route::get('/news', function () {
    $data = file_get_contents(public_path()."\articles.json");
    $data = json_decode($data, true);

    return view('template', ['page'=>'news', 'data' => $data]);
});

Route::get('/about', function () {
    return view('template', ['page'=>'about']);
});

Route::get('/news/{id}', function () {
    return view('template', ['page' => 'new']);
});
?>
