<main class="col-10">
    @php
        $data = file_get_contents(public_path()."\articles.json");
        $data = json_decode($data, true);
        $id = request()->route('id');
        $new = [];
        foreach($data as $obj) {
            if ($obj['id'] == $id) {
                $new = $obj;
            }
        }
    @endphp
    <h1 class='col-12 text-left'>{{ $new['name'] }}</h1>
    <p class='col-12 text-left text-muted'>Опубликовано: {{ $new['date'] }}</p>
    <div class='col-12 d-flex flex-wrap'>
        <p class='col-xl-5 mx-5 my-3 text'>
            {{ $new['desc'] }}
        </p>
        <img class='col-xl-5 col-12 my-3 image' alt="..." src="/img/{{ $new['full_image'] }}">
    </div>
</main>
