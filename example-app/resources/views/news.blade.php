<main class="col-10">
    <h1 class='col-12 text-left'>Статьи</h1>
    <ul class='d-flex gap-3 flex-wrap'>
        @foreach ($data as $article)
            <li>
                <div class="card">
                    <img src='img/{{ $article['preview_image'] }}' class="card-img-top" alt="...">
                    <div class="card-body card__body">
                        <h5 class="card__title">{{ $article['name'] }}</h5>
                        <p class="card__text">{{ $article["shortDesc"] }}</p>
                        <a href="news/{{ $article['id'] }}" class="btn btn-primary btn__read">Читать подробнее</a>
                    </div>
                </div>
            </li>
        @endforeach
    </ul>
</main>
