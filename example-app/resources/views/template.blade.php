<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>about</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
    <link rel='stylesheet' href="/css/style.css">
</head>
<body class='d-flex justify-content-start flex-column col-12 align-content-center align-items-center'>
<header class='d-flex justify-content-center flex-column col-10'>
    <img src="/img/falcon.svg" width="200" alt="falcon" class='align-self-center'>
    <ul class='d-flex flex-wrap justify-content-center col-12'>
        <li class='col-xl-2 col-12 text-center'>
            <a href="/" class='btn btn-primary m-3 col-8'>Основная</a>
        </li>
        <li class='col-xl-2 col-12 text-center'>
            <a href="/news" class='btn btn-primary m-3 col-8'>Новости</a>
        </li>
        <li class='col-xl-2 col-12 text-center'>
            <a href="/about" class='btn btn-primary m-3 col-8'>О нас</a>
        </li>
    </ul>
</header>
@if ($page == "main")
    {{ view("main") }}
@elseif ($page == "news")
    {{ view("news", ['data' => $data]) }}
@elseif ($page == "new")
    {{ view("new") }}
@endif
<footer>
    <h2 class="bottom-text">Соколов Владислав Сергеевич, 211-322</h2>
</footer>
