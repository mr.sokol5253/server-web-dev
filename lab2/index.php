<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Feedback form</title>
    <link rel='stylesheet' href="style.css" />
</head>
<body>
    <header>
        <img class='logo' src='img/LogoMospolytech.jpg' alt="Логотип">
        <h1>"Feedback Form"</h1>
    </header>
    <main>
        <form class='form' action='https://httpbin.org/post' method='POST'>
            <div class='radio'>
                <input class='radio__input' type="radio" name="request" id="complaint" value='complaint'>
                <label class='radio__label' for="complaint">Жалоба</label>
            </div>
            <div class='radio'>
                <input class='radio__input' type="radio" name="request" id="appeal" value='appeal'>
                <label class='radio__label' for="appeal">Обращение</label>
            </div>
            <div class='radio'>
                <input class='radio__input' type="radio" name="request" id="suggestion" value='suggestion'>
                <label class='radio__label' for="suggestion">Предложение</label>
            </div>
            <div class='radio'>
                <input class='radio__input' type="radio" name="request" id="gratitude" value='gratitude'>
                <label class='radio__label' for="gratitude">Благодарность</label>
            </div>
            
            <label class='form__label' for="name">Имя</label>
            <input class='form__item' name="name" type="text" id="name">
            <label class='form__label' for="mail">Почта</label>
            <input class='form__item' type="text" name="mail" id="mail">
            <textarea name="text" class="text_input" type="text"></textarea>
            <input type="checkbox" name="feedback" id="sms" value='sms'>
            <label for="sms">Сообщение</label>
            <input type="checkbox" name="feedback" id="mail-feed" value='mail-feed'>
            <label for="mail-feed">Электронная почта</label>

            <input class='submit' type="submit" value='Отправить'>
            <a class="link" href="header.php">Вторая страница</a>
        </form>
    </main>
    <footer>
    </footer>
</body>
</html>