<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Headers</title>
    <link rel="stylesheet" href="style.css"/>
</head>
<body>
    <form class="form">
        <?php
            $url = "http://httpbin.org";
            $answer = get_headers($url);
        ?>
        <textarea name="" id="" cols="30" rows="20">
            <?php 
                print_r($answer);
            ?>
        </textarea>
        <a class='link 'href="index.php">Назад</a>
    </form> 
   
</body>
</html>